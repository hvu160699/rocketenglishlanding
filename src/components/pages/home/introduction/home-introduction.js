import React from 'react'

const HomeIntroduction = () => {
    return <div className="container pb-3">
        <h3 className="title text-center my-4">Học Tiếng Anh Giống Hệt Cách Bạn Tập Đi Xe Đạp</h3>
        <div className="content-text">
            <div className="text"><p>Hãy nhớ lại: bạn đã từng học đi xe đạp như thế nào?</p><p><strong>Bạn học đi xe đạp bằng cách thực hành.</strong></p><p>Bạn leo lên xe, thử đạp pedal, thử giữ thăng bằng, thử bóp phanh ...</p><p>... ngày qua ngày, bạn tập đi rồi lại tập lại, <strong>tập đi rồi lại tập lại</strong> ...</p><p>... bạn <strong>cải thiện ngay </strong>sau các lần tập.</p><p>... mỗi lần ngã, bạn lại biết cách giữ thăng bằng tốt hơn, đạp xe tốt hơn.</p><p><strong>Rồi cho đến một ngày, bạn thấy đi xe đạp thật quá dễ dàng.</strong></p></div>
            <div className="image text-center mb-5 mt-5">
                <img src="https://cdn-engbreaking.cdn.vccloud.vn/wp-content/uploads/2018/10/01-hoc-tieng-anh-giong-het-cach-ban-tap-xe-dap-min.jpg" className="w-100" />
            </div>
            <div className="text"><p>Chỉ cần <strong>3 tháng thực hành</strong> theo <strong>3 kỹ thuật luyện tập </strong>tiếng Anh độc quyền, Eng Breaking sẽ giúp bạn hoàn toàn có thể <strong>nghe hiểu tiếng Anh ngay</strong> lập tức, <strong>nói tiếng Anh trôi chảy</strong>, <strong>tự tin như người bản xứ</strong>.</p><p>Hãy đọc tiếp, bạn sẽ hiểu <strong>tại sao Eng Breaking lại làm được điều này.</strong></p></div>
        </div>
    </div>
}

export default HomeIntroduction
