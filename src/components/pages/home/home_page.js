import React, { useState, useRef } from 'react';
import "./home_page.scss"
import Introduce from "../../layout/introduce/introduce_layout"
import { Link } from "react-router-dom"
import { redirect_to_order } from "../../../config/router.config"
import Countdown from 'react-countdown-now';
import HomeBanner from './banner/home-banner';
import HomeIntroduction from './introduction/home-introduction';
import HomeReview from './review/home-review';
import HomeStructure from './structure/home-structure';
import HomeVideo from './video/home-video';


const HomePage = () => {

    const [second,] = useState(27543840)
    const [time, setTime] = useState(Date.now() + second)
    const timer = useRef()
    const onComplete = () => {
        setTimeout(() => {
            setTime(Date.now() + second)
            timer.current.start()
        }, 1000);
    }
    return (
        <div className="home-container mb-2">
            {HomeBanner()}
            {Introduce('"Nói Tiếng Anh Trôi Chảy Chỉ Sau 3 Tháng Tự Học!', 'Chương trình đang được hơn 67.200 học viên tại khắp 63 tỉnh thành theo học.')}
            {HomeIntroduction()}
            {Introduce('"Nói Tiếng Anh Trôi Chảy Chỉ Sau 3 Tháng Tự Học!', 'Chương trình đang được hơn 67.200 học viên tại khắp 63 tỉnh thành theo học.')}
            {HomeReview()}
            {Introduce('"Nói Tiếng Anh Trôi Chảy Chỉ Sau 3 Tháng Tự Học!', 'Chương trình đang được hơn 67.200 học viên tại khắp 63 tỉnh thành theo học.')}
            {HomeStructure()}
            {Introduce('"Nói Tiếng Anh Trôi Chảy Chỉ Sau 3 Tháng Tự Học!', 'Chương trình đang được hơn 67.200 học viên tại khắp 63 tỉnh thành theo học.')}
            {HomeVideo()}
            {Introduce('"Nói Tiếng Anh Trôi Chảy Chỉ Sau 3 Tháng Tự Học!', 'Chương trình đang được hơn 67.200 học viên tại khắp 63 tỉnh thành theo học.')}
            <div className="container main-content">
                <div className="content-text">
                    <div className="card text-center mt-3">
                        <div className="card-body">
                            <div className="row row-text">
                                <div className="col-lg-12 col-12 mb-3">
                                    <h5 className="row-text-title">Trọn Bộ Eng Breaking</h5>
                                </div>
                                <div className="col-lg-6 col-12 mb-3">
                                    <img src="https://cdn-engbreaking.cdn.vccloud.vn/wp-content/uploads/2018/10/00-cover-min.png" className="w-100" alt="#" />
                                </div>
                                <div className="col-lg-6 col-12 mb-3">
                                    <ul>
                                        <li className="mb-2"><i className="fa fa-check"></i> Tài khoản <strong>học Online</strong></li>
                                        <li className="mb-2"><i className="fa fa-check"></i> Đĩa DVD <strong>học Offline</strong></li>
                                        <li className="mb-2"><i className="fa fa-check"></i> <strong>Giáo trình in màu</strong></li>
                                        <li className="mb-2"><i className="fa fa-check"></i> Tài khoản <strong>học Online</strong></li>
                                        <li className="mb-2"><i className="fa fa-check"></i> Đĩa DVD <strong>học Offline</strong></li>
                                        <li className="mb-2"><i className="fa fa-check"></i> <strong>Giáo trình in màu</strong></li>
                                    </ul>
                                </div>
                                <hr />
                            </div>

                            <div className="countdown mt-3 mb-3">
                                <h5 className="mb-4">Miễn Phí Vận Chuyển Đến Thứ 5 Ngày 4/07</h5>
                                <h5 className="mb-4">Giá Gốc <span>990.000 VNĐ</span> </h5>
                                <h5 className="mb-4">Khuyến Mãi 51% Chỉ Còn <strong>490.000 VNĐ</strong> Kết Thúc Trong</h5>
                                <div className="count">
                                    <Countdown renderer={renderer} ref={timer} onComplete={onComplete} date={time} />
                                </div>
                            </div>

                        </div>
                    </div>
                    <Link className="btn-main link-btn-main mt-3 mb-3" to={redirect_to_order()}>Tới trang đặt hàng ngay >></Link>
                    <h6 className="text-center mb-3">Nhanh lên! chỉ còn <strong style={{ color: '#E5372B' }}>18</strong> Bộ Sản Phẩm.</h6>
                    <img src="https://cdn-engbreaking.cdn.vccloud.vn/wp-content/uploads/2018/12/181207-logo-doi-tac.png" className="w-100" alt="#" />
                </div>
            </div>
        </div>
    );
};


const renderer = ({ total, days, hours, minutes, seconds, completed }) => {
    if (completed) {
        return <span>{0}:{0}:{0}</span>;
    } else {
        // Render a countdown
        return <span>{addZero(hours)}:{addZero(minutes)}:{addZero(seconds)}</span>;
    }
};

const addZero = i => {
    if (i < 10) {
        i = `0${i}`;
    }
    return i;
}


export default HomePage;