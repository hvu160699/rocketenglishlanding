import * as React from 'react';
import './index.css';
import vu from './vu.png';
import thuan from './thuan.png';
import qui from './qui.png';
const HomeFeedback = () => {
    return (
        <div className="container feedback-container">
            <h2 className="feedback-title">Cảm nhận của học viên</h2>
            <div className="row">
                <div className="col-lg-4 feedback-item">
                    <div className="feedback-item-info">
                        Bạn Dương Hoàng Vũ , 20 tuổi, Sinh viên
                    </div>
                    <div className="feedback-item-box-img">
                        <img className="img-fluid" src={vu} alt="" />
                    </div>
                    <div className="feedback-item-feedback">
                        <p className="block-with-text">
                            Công việc lại quá bận rộn làm mình lười đến trung tâm học. Cháu gái mình đã giới thiệu mình RocketEnglish, thật sự mình thấy tuyệt vời hơn cả những gì cháu gái mình đã nói. Phương pháp học rất hay, mình tiến bộ trông thấy.
                        </p>
                    </div>
                </div>
                <div className="col-lg-4 feedback-item">
                    <div className="feedback-item-info">
                        Bạn Dương Hoàng Vũ , 20 tuổi, Sinh viên
                    </div>
                    <div className="feedback-item-box-img">
                        <img className="img-fluid" src={thuan} alt="" />
                    </div>
                    <div className="feedback-item-feedback">
                        <p className="block-with-text">
                            Em học tiếng Anh từ lớp 6 tới giờ nhưng vẫn không thể nào giao tiếp được dù là cơ bản, em đã biết đến RocketEnglish nhờ vào quảng cáo trên Facebook. Em đã thử mua về học so với học phí ở trung tâm thì khóa học này khá rẻ. Em lấy lại căn bản một cách nhanh chóng, Sau khi kết thúc 3 tháng em đã giao tiếp khá tốt. cảm ơn RocketEnglish rất nhiều
                        </p>
                    </div>
                </div>
                <div className="col-lg-4 feedback-item">
                    <div className="feedback-item-info">
                        Bạn Dương Hoàng Vũ , 20 tuổi, Sinh viên
                    </div>
                    <div className="feedback-item-box-img">
                        <img className="img-fluid" src={qui} alt="" />
                    </div>
                    <div className="feedback-item-feedback">
                        <p className="block-with-text">
                            Tôi chạy xe ôm thì khách rất đa dạng người Việt có, người nước ngoài có. mà vì tôi không biết Tiếng anh nên có lúc bỏ lỡ rất nhiều khách nước ngoài. Tôi biết đến RocketEnglish qua thằng bé khách quen. Bây giờ tôi có thể giao tiếp với người nước ngoài những câu cơ bản và biết họ đang nói gì. Nhiều khi tôi còn giúp mấy người bán hàng rong phiên dịch lại nữa.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HomeFeedback;