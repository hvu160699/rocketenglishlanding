import React from 'react';
import './index.css';
const HomeIntroduction = () => {
    return (
        <div className="introduction-container">
            <div className="row">
                <div className="col-lg-6 introduction-item">
                    <div className="introduction-icon-container">
                        <div className="introduction-icon">
                            <span className="introduction-icon-content">
                                36
                                </span>
                        </div>
                    </div>
                    <div className="introduction-content">
                        <h3 className="introduction-content-title">
                            Chủ đề giao tiếp thông dụng hàng ngày
                        </h3>
                        <div className="introduction-content-p">
                            Học tiếng Anh theo chủ đề là phương pháp hiệu quả đang được nhiều người áp dụng hiện nay. Với 36 chủ đề tiếng Anh thông dụng được tổng hợp này sẽ giúp bạn tự tin, vận dụng được trong các tình huống hàng ngày.
                        </div>
                    </div>
                </div>
                <div className="col-lg-6 introduction-item">
                    <div className="introduction-icon-container">
                        <div className="introduction-icon">
                            <span className="introduction-icon-content">
                                259
                                </span>
                        </div>
                    </div>
                    <div className="introduction-content">
                        <h3 className="introduction-content-title">
                            Từ vựng được sử dụng phổ biến thường gặp
                        </h3>
                        <div className="introduction-content-p">
                            Nhờ những kỹ thuật RocketEnglish sẽ giúp bạn tự động nhớ 259 từ vựng này mà không cần phải cố gắn học thuộc, và hệ thống lại những từ vựng đã học trong ngày
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-6 introduction-item">
                    <div className="introduction-icon-container">
                        <div className="introduction-icon">
                            <span className="introduction-icon-content">
                                216
                                </span>
                        </div>
                    </div>
                    <div className="introduction-content">
                        <h3 className="introduction-content-title">
                            Câu sử dụng nhiều nhất trong đời sống
                        </h3>
                        <div className="introduction-content-p">
                            216 mẫu câu tiếng Anh này, chúng không chỉ giúp bạn ứng phó nhanh khi đối thoại, mà còn diễn đạt tốt và tự nhiên hơn
                            </div>
                    </div>
                </div>
                <div className="col-lg-6 introduction-item">
                    <div className="introduction-icon-container">
                        <div className="introduction-icon">
                            <span className="introduction-icon-content">
                                72
                            </span>
                        </div>
                    </div>
                    <div className="introduction-content">
                        <h3 className="introduction-content-title">
                            Audio chuẩn giúp bạn luyện nghe và nói tốt nhất
                        </h3>
                        <div className="introduction-content-p">
                            Giúp bạn phát âm đúng nhất, giao tiếp tự nhiên như người bản xứ sau khi hoàn thành khóa học
                            </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HomeIntroduction;