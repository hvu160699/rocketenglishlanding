import * as React from 'react';
import './index.css';
const HomeOrder = () => {
    return (
        <div className="order-container">
            <div className="container">
                <div className="row">
                    <div className="col-md-8">
                        <h2 className="order-title">Bứt phá tiếng Anh</h2>
                        <div className="order-content">Bứt phá tiếng Anh theo phương pháp Rocket English</div>
                    </div>
                    <div className="col-md-4 order-link-wrapper">
                        <a className="order-link" href="#" target="_blank">Đặt hàng ngay</a>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default HomeOrder;