import React from 'react';
import group from "./group.png";
import listen from "./listen.png";
import speak from "./speak.png";
import check from "./check.png";
import './index.css';
import ImgCenter from '../img-center';
const HomeArticle = () => {
    return (
        <div className="container article-container">
            <h1 className="article-title">
                Phương pháp học
            </h1>
            <div className="article-item">
                <h3 className="article-item-title">
                    Immersion_Phương pháp thẩm thấu tự nhiên
                </h3>
                <p className="article-item-p">
                    Phương pháp thẩm thấu tự nhiên là một cách phổ biến để hỗ trợ tăng cường các phương thức của việc học, nhưng dường như nó hoạt động đặc biệt tốt đối với việc học các ngôn ngữ, bằng cách đặt học sinh vào trong một ngữ cảnh, xa khỏi môi trường của lớp học, nơi chúng được hòa mình vào một môi trường nghiên cứu ngôn ngữ được cất lên.
                </p>
                <p className="article-item-p">
                    Đi nghỉ không có gì đáng kể, nhưng nếu nói đi du lịch đến Vương quốc Anh, cho một kỳ thực tập hoặc một khóa học ngắn là lý tưởng - cho bạn cơ hội lắng nghe và học hỏi trong công việc, được bao quanh bởi ngôn ngữ trong chính bối cảnh tự nhiên của ngôn ngữ đó không phải lớp học mỗi ngày.
                </p>
            </div>
            <div className="article-item">
                <h3 className="article-item-title">
                    Distributed Practice_Phân bổ thời gian ôn tập
                </h3>
                <p className="article-item-p">
                    Phân bổ thời gian ôn tập, một thuật ngữ mới cho một số thứ mà nó đã được thực hiện trong một thời gian dài, về cơ bản là thực hiện việc học của bạn “một chút và thường xuyên”. Các nghiên cứu đã chỉ ra rằng nhồi nhét marathon hoặc tiêu thụ quá nhiều tài liệu học tập trong một khoảng thời gian ngắn không mang lại hiệu quả cho việc học tập lâu dài, vì thông tin không đến được các phần sâu của não.
                </p>
                <p className="article-item-p">
                    Thay vì dành một ngày một tuần cho các nghiên cứu tiếng Anh của bạn, tốt hơn là thực hiện nửa giờ mỗi ngày: kỹ thuật của việc phân bổ luyện tập trong một khoảng thời gian này, sẽ chia thành các phần nhỏ, có nhiều khả năng gặt hái kết quả hơn. Các thử nghiệm phát hiện ra rằng bạn muốn thông tin tồn tại càng lâu thì khoảng thời gian càng dài; Vì vậy, khi nói đến việc xem xét khối lượng lớn công việc, hãy từ từ dành thời gian của bạn!
                </p>
            </div>
            <div className="article-item">
                <h3 className="article-item-title">
                    Distributed Practice_Phân bổ thời gian ôn tập
                </h3>
                <p className="article-item-p">
                    Một nghiên cứu học thuật gần đây từ Hoa Kỳ cho thấy kiểm tra thực hành cũng là một cách tuyệt vời để cải thiện việc học của bạn. Tại sao? Không phải để tăng mức độ căng thẳng, mà bởi vì các bài kiểm tra thực hành yêu cầu một hoạt động của não điều mà sẽ khác với khi chúng ta đơn giản tiếp thu thông tin - nó thách thức khả năng của bộ não để nhớ lại và đào thông tin được lưu trữ trước đó và kết hợp nó một cách sáng tạo.
                </p>
                <p className="article-item-p">
                    Bạn có thể thực hành bằng cách hoàn thành các bài học trong chương trình học của mình hoặc đặt cho mình một nhiệm vụ thực hành từ vựng bạn đã học mỗi tuần bằng cách viết một văn bản nhỏ mà không cần tham khảo bất kỳ tài liệu nào của bạn.
                </p>
            </div>
            <ImgCenter src={group}/>
            <div className="row">
                <div className="col-lg-4">
                    <div className="article-skill-container">
                        <div className="article-skill-box-img">
                            <img src={listen} alt="" />
                        </div>
                        <h3 className="article-skill-title">Nghe cảm nhận</h3>
                        <div className="article-skill-description">Nghe - Hiểu cảm xúc của câu, cảm nhận từng âm thanh, ngữ điệu cũng như hiểu được nội dung của cả bài</div>
                    </div>
                </div>
                <div className="col-lg-4">
                    <div className="article-skill-container">
                        <div className="article-skill-box-img">
                            <img className="img-fluid" src={speak} alt="" />
                        </div>
                        <h3 className="article-skill-title">Nói bắt chước</h3>
                        <div className="article-skill-description">Luyện nói giúp tạo ra những liên kết trong não bộ khi thiết lập những âm thanh, từngữ và câu một cách nhanh nhất, như một phản xạ thần kinh.</div>
                    </div>
                </div>
                <div className="col-lg-4">
                    <div className="article-skill-container">
                        <div className="article-skill-box-img">
                            <img className="img-fluid" src={check} alt="" />
                        </div>
                        <h3 className="article-skill-title">Kiểm tra</h3>
                        <div className="article-skill-description">Hệ thống lại bài học, có môi trường luyện tập nhiều hơn</div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HomeArticle;