import React from 'react';
import item from "./item.png";
import './index.css';
const Feature = () => {
    return (
        <div className="container">
            <div className="row">
                <div className="col-lg-4">
                    <div className="feature-container">
                        <div className="feature-box-img">
                            <img className="img-fluid" src={item} alt="" />
                        </div>
                        <h3 className="feature-title">Tài khoản học online</h3>
                        <div className="feature-description">
                            Giải pháp cho người bận rộn, bạn có thể dễ dàng truy cập vào điện thoại, các thiết bị công nghệ...
                        </div>
                    </div>
                </div>
                <div className="col-lg-4">
                    <div className="feature-container">
                        <div className="feature-box-img">
                            <img className="img-fluid" src={item} alt="" />
                        </div>
                        <h3 className="feature-title">Giáo trình in màu</h3>
                        <div className="feature-description">
                            Dễ dàng học ngay cả khi không thể kết nối Internet, ôn tập lại kiến thức
                        </div>
                    </div>
                </div>
                <div className="col-lg-4">
                    <div className="feature-container">
                        <div className="feature-box-img">
                            <img className="img-fluid" src={item} alt="" />
                        </div>
                        <h3 className="feature-title">Bộ thẻ từ vựng Flashcard</h3>
                        <div className="feature-description">
                            Hỗ trợ ghi nhớ từ, câu, ví dụ nhanh hơn và lâu hơn. Với lợi thế nhỏ gọn và dễ mang theo để học bất cứ lúc nào.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Feature;