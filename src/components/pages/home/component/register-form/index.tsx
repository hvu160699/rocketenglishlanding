import * as React from 'react';
import './index.css';
const HomeOrder = () => {
    return (
        <div className="container register-form-container">
            <div className="row">
                <div className="col-lg-6">
                    <h2 className="register-form-title">Đăng ký tại đây</h2>
                    <div className="register-form-description">
                        Bạn điền đầy đủ thông tin và kiểm tra lại SĐT, địa chỉ của bạn lại lần nữa nhé.
                        </div>
                    <h4 className="register-form-combo-title">Trọn bộ khóa học Rocket English</h4>
                    <ul className="register-form-combo">
                        <li className="register-form-combo-item">Tài khoản học Online</li>
                        <li className="register-form-combo-item">Giáo trình in màu</li>
                        <li className="register-form-combo-item">To do list</li>
                        <li className="register-form-combo-item">Sticky Note</li>
                        <li className="register-form-combo-item">Hướng dẫn sử dung khóa học</li>
                        <li className="register-form-combo-item">Email nhắc nhở và hỗ trợ mỗi ngày</li>
                    </ul>
                </div>
                <div className="col-lg-6">
                    <div className="form-group">
                        <label>Họ và tên</label>
                        <input className="form-control" />
                    </div>
                    <div className="form-group">
                        <label>Tuổi của bạn</label>
                        <input className="form-control" />
                    </div>
                    <div className="form-group">
                        <label>Số điện thoại</label>
                        <input className="form-control" />
                    </div>
                    <div className="form-group">
                        <label>Địa chỉ</label>
                        <input className="form-control" />
                    </div>
                    <div className="form-group">
                        <label>Email</label>
                        <input className="form-control" />
                    </div>
                    <button type="button" className="register-form-submit">Gửi</button>
                </div>
            </div>
        </div>
    )
}
export default HomeOrder;