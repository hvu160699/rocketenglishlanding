import React from 'react';
import './index.css';
interface IImgCenterProps {
    src: string;
}
const ImgCenter = (props: IImgCenterProps) => {
    return (
        <div className="container img-center">
            <img className="img-fluid mx-auto" src={props.src} alt="" />
        </div>
    )
}

export default ImgCenter;