import React from 'react'

const HomeStructure = () => {
    return (
        <div className="container pb-3">
            <h3 className="title text-center my-4">Cấu Trúc Khóa Học Tiếng Anh Online Eng Breaking</h3>
            <p className="title-child text-center mb-5">Gồm tài khoản học Online và ban giáo vụ hỗ trợ, nhắc nhở việc học hàng tuần và giải đáp thắc mắc</p>
            <div className="content-text">
                <img src="https://cdn-engbreaking.cdn.vccloud.vn/wp-content/uploads/2018/12/08-banner-cau-truc-khoa-hoc.png" className="w-100" alt="banner" />
            </div>
            <h3 className="title text-center mt-3 mb-5">Những Tính Năng Nổi Bật Mà Chỉ Eng Breaking Mới Có</h3>
            <div className="row text-center row-text">
                <div className="col-lg-4 col-12 mb-4">
                    <div className="image"><img className="w-50" src="https://cdn.engbreaking.com/wp-content/uploads/2019/01/thacmac.png" /></div>
                    <div className="content">
                        <h5 className="row-text-title mb-4 mt-4">Ban Giáo Vụ Hỗ Trợ Đa Kênh</h5>
                        <p>
                            Ban giáo vụ <strong>giải đáp mọi thắc mắc cho bạn</strong> đa kênh <strong> qua email, messenger, hotline, ...</strong>
                        </p>
                    </div>
                </div>
                <div className="col-lg-4 col-12 mb-4">
                    <div className="image"><img className="w-50" src="https://cdn.engbreaking.com/wp-content/uploads/2017/03/checklist.jpg" /></div>
                    <div className="content">
                        <h5 className="row-text-title mb-4 mt-4">Checklist Công Việc</h5>
                        <p>
                            Toàn bộ <strong> checklist công việc</strong> cần hoàn thành hàng ngày
                        </p>
                    </div>
                </div>
                <div className="col-lg-4 col-12 mb-4">
                    <div className="image"><img className="w-50" src="https://cdn.engbreaking.com/wp-content/uploads/2017/03/icon-circle-email-a0d3582419dad629ae58c6df5d84413e201512991925.jpg" /></div>
                    <div className="content">
                        <h5 className="row-text-title mb-4 mt-4">Hệ Thống Email Hỗ Trợ</h5>
                        <p>
                            Hệ thống  <strong> email hỗ trợ, đồng hành</strong> suốt thời gian luyện tập
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HomeStructure
