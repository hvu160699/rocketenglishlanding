import React, { useState, useEffect } from 'react';
import * as Notify from "../../../helpers/notify_helper"
import { validatorEmail, validatorRequired } from "../../../helpers/validator_helper"
import { postContact } from "../../../api/contact/contact_api"
import "./order_page.scss"
import dataVietNam from '../../../helpers/data.json'
import * as path from "../../../config/router.config"
import Banner from '../../layout/banner/banner_layout';
import { Select, Form, Input, Tooltip, Icon } from 'antd';
const { Option } = Select;

    

const OrderPageCom = (props) => {
    const { getFieldDecorator, form } = props.form;

    const [dataCity, setDataCity] = useState()
    const [loadingCity, setLoadingCity] = useState(false)
    const [dataDistrict, setDataDistrict] = useState([])
    const [dataProvide, setDataProvide] = useState([])



    useEffect(() => {
        setDataCity(dataVietNam)
        return () => {

        };
    }, [])


    const Order = e => {
        const form = props.form;
        form.validateFieldsAndScroll(async (err, values) => {
            if (!err) {
                try {
                    const data = await postContact(values).catch(err => console.log(err))
                    if (data === 'CREATE_SUCCESS') {
                        Notify.successPost('Đặt mua thành công.')
                    } else Notify.error('Đã có lỗi xảy ra, vui lòng thử lại.')
                } catch (error) {
                    console.log(error)
                }
            }
        });
    }

    // const handleChange = e => {
    //     const { name, value } = e.target
    //     setInfoUser({ [name]: value });
    // }

    const handleChangeCity = value => {
        setLoadingCity(true)
        setDataProvide([])
        const result = dataCity;
        let ok = result.filter(elm => elm.name === value);
        setDataProvide(ok[0].districts)
        setTimeout(() => {
            setLoadingCity(false)
        }, 500);
    }

 


    return (

        <React.Fragment>
            {Banner('',path.redirect_to_order(),"")}
            <div className="order-container mt-5 mb-5">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 col-12">
                            <h4>Thông Tin Đặt Hàng</h4>
                            <p> Hơn <strong>80.000</strong> học viên là người mới bắt đầu, mất gốc, bận rộn đang theo học. </p>
                            <Form  onSubmit={Order} className="mt-3">     
                                <Form.Item>
                                    {getFieldDecorator("name", {
                                        rules: [
                                            { required: true, message: validatorRequired }
                                        ]
                                    })(
                                        <Input
                                            size="large"
                                            prefix={
                                                <Tooltip placement="leftTop" title="Họ tên">
                                                    <Icon type="user" className="courses-register-icon" />
                                                </Tooltip>
                                            }
                                            placeholder="Họ tên*"
                                        />
                                    )}
                                </Form.Item>
                                <Form.Item>
                                    {getFieldDecorator("phone", {
                                        rules: [
                                            { required: true, message: validatorRequired }
                                        ]
                                    })(
                                        <Input
                                            size="large"
                                            prefix={
                                                <Tooltip placement="leftTop" title="Điện thoại">
                                                    <Icon type="phone" />
                                                </Tooltip>
                                            }
                                            placeholder="Điện thoại*"
                                        />
                                    )}
                                </Form.Item>
                                <Form.Item>
                                    {getFieldDecorator("email", {
                                        rules: [
                                            { type: "email", message: validatorEmail },
                                            { required: true, message: validatorRequired }
                                        ]
                                    })(
                                        <Input
                                            size="large"
                                            prefix={
                                                <Tooltip placement="leftTop" title="Email">
                                                    <Icon type="mail" />
                                                </Tooltip>
                                            }
                                            placeholder="Email*"
                                        />
                                    )}
                                </Form.Item>
                                <div className="row">
                                    <div className="col-sm-6">
                                        <Form.Item>
                                            {getFieldDecorator("city", {
                                                rules: [
                                                    { required: true, message: validatorRequired }
                                                ]
                                            })(
                                                <Select size="large" onChange={handleChangeCity} placeholder="Tỉnh - Thành Phố" >
                                                    {/* <Option value="city">Tỉnh - Thành Phố</Option> */}
                                                    {dataCity && dataCity.map(elm => {
                                                        return <Option value={elm.name}>{elm.name}</Option>
                                                    })}
                                                </Select>
                                            )}
                                        </Form.Item>
                                    </div>
                                    <div className="col-sm-6">
                                        <Form.Item>
                                            {getFieldDecorator("provide", {
                                                rules: [
                                                    { required: true, message: validatorRequired }
                                                ]
                                            })(
                                                <Select size="large" placeholder="Quận - Huyện" loading={loadingCity ? true : false} >
                                                 {/* <Option value="city">Quận - Huyện</Option> */}
                                                    {dataProvide && dataProvide.map(elm => <Option value={elm}>{elm}</Option>)}
                                                </Select>
                                            )}
                                        </Form.Item>
                                    </div>
                                </div>
                                <Form.Item>
                                    {getFieldDecorator("address", {
                                        rules: [
                                            { required: true, message: validatorRequired }
                                        ]
                                    })(
                                        <Input
                                            size="large"
                                            prefix={
                                                <Tooltip placement="leftTop" title="Địa chỉ">
                                                    <Icon type="home" />
                                                </Tooltip>
                                            }
                                            placeholder="Địa chỉ*"
                                        />
                                    )}
                                </Form.Item>
                            </Form>
                            <div className="mt-3 bg-grey">
                                <div className="row">
                                    <div className="col-lg-5 col-12"><p>Giá Gốc: <span>990.000 VNĐ</span></p></div>
                                    <div className="col-lg-7 col-12"><h6>Khuyến Mãi 51% Còn: 490.000 VNĐ</h6></div>
                                </div>
                            </div>
                            <h6 className="text-center mt-3 mb-3" style={{ color: 'red' }}>Nhanh Lên! Chỉ Còn 8 Bộ Sản Phẩm và Quà Tặng!</h6>
                            <button onClick={Order} className="btn-main link-btn-main mt-3 mb-3">Đặt hàng ngay</button>
                            <div className="mt-3 bg-grey">
                                <h6 className="text-center" style={{ color: '#008000' }}>11 Người Đang Theo Dõi Trang Đặt Hàng!</h6>
                            </div>
                            <img src="https://cdn-engbreaking.cdn.vccloud.vn/wp-content/uploads/2017/10/2017-10-12_1801-1.png" className="w-100 mt-3" />
                        </div>
                        <div className="col-lg-4 col-12">
                            <div className="hotline text-center text-uppercase mb-4"> Hotline: 070 7966679 </div>
                            <div className="image mb-4">
                                {/* <img className="w-100" src="https://cdn-engbreaking.cdn.vccloud.vn/wp-content/uploads/2018/12/00-cover-min.png" /> */}
                                <div style={{width:'300px',height:'150px' ,background:'grey' }} className="m-auto"></div>
                            </div>
                            <div className="ulli mt-5 mb-5">
                                <h5 className="text-center mb-3">Trọn bộ Clevertube</h5>
                                <ul>
                                    <li className="mb-3"><i className="fa fa-check"></i> Tài khoản học Online</li>
                                    <li className="mb-3"><i className="fa fa-check"></i> Giáo trình Clevertube In Màu</li>
                                    <li className="mb-3"><i className="fa fa-check"></i> Giáo trình Clevertube In Màu</li>
                                    <li className="mb-3"><i className="fa fa-check"></i> Sổ tay kế hoạch hành động</li>
                                    <li className="mb-3"><i className="fa fa-check"></i> Email nhắc nhở và hỗ trợ mỗi ngày</li>
                                    <li className="mb-3"><i className="fa fa-check"></i> Ban giáo vụ đồng hành 24 / 7</li>
                                    <li className="mb-3"><i className="fa fa-check"></i> Thẻ mục tiêu</li>
                                </ul>
                            </div>
                            <div className="ulli mt-5 mb-5">
                                <h5 className="text-center mb-3">Bộ quà tặng trị giá 795.000 VNĐ</h5>
                                <ul>
                                    <li className="mb-3"><i className="fa fa-check"></i>36 chủ đề giao tiếp nâng cao</li>
                                    <li className="mb-3"><i className="fa fa-check"></i> Bộ Flashcard Online học từ vựng</li>
                                    <li className="mb-3"><i className="fa fa-check"></i> Bộ 509 câu giao tiếp thông dụng nhất</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </React.Fragment>
    );
};



const OrderPage = Form.create({ name: "orderForm" })(OrderPageCom);

export default OrderPage;
