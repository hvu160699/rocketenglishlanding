import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom"
import * as path from "../../../config/router.config"

import * as API_STORY from "../../../api/story/story_api"
import * as API_HEADER from "../../../api/header/header_api"
import { HOST_STATIC } from "../../../config/linkAPI.config"
import { returnTitle, convertDate, convertToHOST } from "../../../helpers/convert_helper"
import { success } from "../../../helpers/notify_helper"
import Loading from "../../../helpers/loading_helper"
import "./tips_page.scss"
import Banner from '../../layout/banner/banner_layout';

const TipsPage = (props) => {
    const [pageName, setPageName] = useState()
    const [arrTips, setArrTips] = useState([])
    const [loading, setLoading] = useState(true)
    const getMenuByMetatitle = async () => {
        try {
            const menu = await API_HEADER.getMenuByMetaTitle(props.match.path.split('/')[1]).catch(err => console.log(err))
            const post = await API_STORY.getPostByMenu(menu.id).catch(err => console.log(err))
            setPageName(menu.name)
            post && setArrTips(post)
            post && setLoading(false)
        } catch (err) {
            console.log(err)
        }
    }


    useEffect(() => {
        getMenuByMetatitle()
    }, [])

    const printArrTips = arrTips => {
        return arrTips.map(tip => {
            return <div className="col-lg-4 col-sm-6 col-12 mb-5">
                <Link to={path.redirect_to_tips_detail(tip.metaTile)} title={tip.name} className="content-each-box">
                    <div className="thumbnail-each-box">
                        <div>
                            <img className="w-100" title={tip.name} src={convertToHOST(tip.thumbnail)} alt={tip.thumbnail} />
                        </div>
                        <div className="success-stories">{pageName}</div>
                    </div>
                    <div className="title-each-story">{returnTitle(tip.name)}</div>
                    <div className="footer-each-box">
                        <span className="span-author">
                            <span className="author-name-each-box">{tip.createdBy ? tip.createBy : 'Admin'}</span>
                            <span className="space-span"> - </span>
                        </span>
                        <span className="span-post-date"> {convertDate(tip.createdDate)}</span>
                    </div>
                </Link>
            </div>
        })
    }



    return (
        <React.Fragment>
            {Loading(loading)}
            {Banner('',path.redirect_to_tips(),"Mẹo học tiếng anh")}
            <div className="story-container mt-5 mb-5">
                <div className="container">
                    {/* <p className="stories-title pb-5">Mẹo học tiếng anh</p> */}
                    <div className="row mt-5">
                        {
                            arrTips && printArrTips(arrTips)
                        }
                    </div>
                    <div className="pagination">
                        <a href="#" className="pagination-text">«</a>
                        <a href="#" className="pagination-text">1</a>
                        <a href="#" className="pagination-text">»</a>
                    </div>

                </div>
            </div>
        </React.Fragment>
    );
};

export default TipsPage;