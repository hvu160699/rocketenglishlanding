import React, { useState, useEffect, useReducer } from 'react';
import { Redirect, Link } from "react-router-dom"
import "./story_detail_page.scss"
import { redirect_to_story_detail, redirect_to_order } from "../../../../config/router.config"
import * as path from "../../../../config/router.config"
import * as API_STORY from "../../../../api/story/story_api"
import * as API_HEADER from "../../../../api/header/header_api"
import { HOST_STATIC } from "../../../../config/linkAPI.config"
import { returnTitle, convertDate } from "../../../../helpers/convert_helper"
import Loading from "../../../../helpers/loading_helper"
import Banner from '../../../layout/banner/banner_layout';

const StoryDetailPage = (props) => {
    const [menu, setMenu] = useState(props.match.path.split('/')[1])
    const [detailPost, setDetailPost] = useState()
    const [loading, setLoading] = useState(true)
    const [arrStories, setArrStories] = useState([])
    const [replace,setReplace] = useState(true)
    const getPostDetail = async (title) => {
        setLoading(true)
        try {
            const detailPost = await API_STORY.getPostByMetaTitle(title).catch(err => console.log(err))
            detailPost && setLoading(false)
            detailPost && setDetailPost(detailPost)
        } catch (error) {
            console.log(error)
        }
    }

    const getMenu = async () => {
        setLoading(true)
        try {
            const menu = await API_HEADER.getMenuByMetaTitle(props.match.path.split('/')[1]).catch(err => console.log(err))
            const post = await API_STORY.getPostByMenu(menu.id).catch(err => console.log(err))
            post && setArrStories(post)
            post && setLoading(false)
        } catch (error) {
            console.log(error)
        }
    }


    useEffect(() => {
        getPostDetail(props.match.params.metaTitle)
        getMenu()
        return () => { };
    }, [])

    const [infoComment, setInfoComment] = useReducer((state, newState) => ({ ...state, ...newState }), {});
    const postCommemt = () => {
        if (infoComment != null) alert("Nhap thong tin")
    }

    const handleChange = e => {
        const { name, value } = e.target
        setInfoComment({ [name]: value });
    }

    const changeStory = async title => {
        props.match.params.metaTitle !== title ?  getPostDetail(title) : setReplace(false)
    }

    const printDetailPost = detailPost => {
        return <div className="story-detail">
            <div className="content mb-3 mt-3">
                {/* <h3 className="text-center">{detailPost.name}</h3> */}
                <div dangerouslySetInnerHTML={{ __html: detailPost.content }}></div>
            </div>

            <div className="footer-story-detail">
                <div className="image text-center m-auto">
                    <img className="d" src="https://cdn-engbreaking.cdn.vccloud.vn/wp-content/uploads/2017/09/9-14-2017-10-16-30-AM.png" />
                </div>
                <Link to={redirect_to_order()} replace={replace} className="btn-main link-btn-main mt-3">Tìm hiểu Clevertube Ngay >></Link>
            </div>
            {/* <div className="form-contact mt-5">
                <h6 className="form-title mb-3 mt-3">LEAVE A REPLY</h6>
                <form className="form-group">
                    <textarea onChange={e => handleChange(e)} name="comment" className="form-control mb-3" cols={20} rows={10} placeholder="Comment:" />
                    <input onChange={e => handleChange(e)} name="name" type="text" className="form-control mb-3" placeholder="Name: *" required />
                    <input onChange={e => handleChange(e)} name="email" type="email" className="form-control mb-3" placeholder="Email: *" required />
                    <input onChange={e => handleChange(e)} name="website" type="text" className="form-control mb-3" placeholder="Website: *" required />
                    <button onClick={postCommemt} className="btn-main">Post Comment</button>
                </form>
            </div> */}
        </div>
    }


    const printRecentStory = data => {
        return (
            <div className="item-right mb-lg-5 ">
                <h4 className="title mb-4">BÀI VIẾT KHÁC</h4>
                {
                    data.map(story =>
                        <div className="list-post mb-5 mt-3">
                            <Link to={redirect_to_story_detail(story.metaTile)} onClick={() => changeStory(story.metaTile)} class="row">
                                <div className="col-lg-5 col-4 ">
                                <img className="w-100" title={story.name} src={`${HOST_STATIC}${story.thumbnail}`} alt={story.thumbnail} />
                                </div>
                                <div className="col-lg-7 col-8 post-item">
                                    <a>
                                        <div className="item">
                                            <div className="title-post">{returnTitle(story.name)}</div>
                                            {/* <div className="price">$49.00
                            <span className="old-price"> $65.00</span>
                                            </div> */}
                                        </div>
                                    </a>
                                </div>
                            </Link>
                        </div>
                    )

                }
            </div>
        )
    }




    // detailPost.thumbnail
    return (
        <React.Fragment>
            {Loading(loading)}
            {detailPost && Banner('', props.match.params.metaTitle, detailPost.name)}
            <div className="story-detail-container mt-5 mb-5">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 col-12 mb-5">
                            {
                                detailPost && printDetailPost(detailPost)
                            }
                        </div>
                        <div className="col-lg-3 offset-lg-1 col-12">
                                {
                                    arrStories && printRecentStory(arrStories)
                                }
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>

    );
};
export default StoryDetailPage;