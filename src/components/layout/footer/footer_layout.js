import React from 'react';
import "./footer_layout.scss"

const Footer = () => {
  return (
    <div className="sub-footer">
      <div className="container">
        <div className="row">
          <div className="col-lg-3 col-12 d-lg-block d-none">
          <span className="copyright"><i className="fa fa-copyright"></i> English landing page</span>
          </div>
          <div className="col-lg-9 col-12 ml-auto">
          <ul className="sub-right">
            <li>Quy định chung</li>
            <li>Chính sách bảo mật</li>
            <li>Liên hệ</li>
          </ul>
          </div>
          <div className="col-lg-3 col-12 d-block d-lg-none">English landing page</div>
        </div>
      </div>
    </div>
  );
};

export default Footer;