import React, { useEffect } from 'react';
import { Link } from "react-router-dom"
import "./back_to_top.scss";
import { redirect_to_order } from "../../../config/router.config"

const BackToTop = () => {
    useEffect(() => {
        const backToTop = document.getElementById("back-to-top");
        document.addEventListener("scroll", function () {
            const st = document.body.scrollTop || document.documentElement.scrollTop
            if (st > 200) backToTop.classList.add("scrolldown")
            else backToTop.classList.remove("scrolldown")
        }, false);

        return () => {
            document.removeEventListener("scroll")
        };
    }, [])

    const scrollTop = () => {
        window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });
    }

    return <React.Fragment>
        <div id="back-to-top" onClick={scrollTop}><i className="fa fa-chevron-up" /></div>
        <div id="order-now" className="btn-order-now">
            <Link to={redirect_to_order()}>Đặt hàng ngay <i className="fas fa-arrow-right"></i></Link>
        </div>
    </React.Fragment>
};

export default BackToTop;