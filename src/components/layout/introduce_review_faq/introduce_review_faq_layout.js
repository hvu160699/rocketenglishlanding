import React from 'react';
import "./introduce_review_faq_layout.scss"

const IntroduceReviewFaq = (data) => {
    return (
        <React.Fragment>
            <div className="row text-center">
                <div className="col-lg-6 col-12 mb-4">
                    <p className="title-introduce mt-3 mb-3">Hello From US</p>
                    <img className="w-100" src={require('../../../assets/images/vid1.jpg')} alt="" />
                </div>
                <div className="col-lg-6 col-12 mb-4">
                    <p className="title-introduce mt-3 mb-3">About Us</p>
                    <img className="w-100" src={require('../../../assets/images/vid2.jpg')} alt="" />
                </div>
            </div>
            <h6 className="text-center title-introduce mt-5">Sự Thành Công Của Bạn Là Sứ Mệnh Của Chúng Tôi</h6>
            <div className="image text-center">
                <img className="w-100" src={require('../../../assets/images/review-people2.png')} alt="" />
            </div>
            <div className="footer-review mt-3">
                <h6 className="title-introduce">Có Thông Tin Trên</h6>
                <div className="image text-center">
                    <img className="w-100" src={require('../../../assets/images/news.png')} alt="" />
                </div>
                <h6 className="title-introduce mt-5">Phá Tan Nỗi Sợ Giao Tiếp Tiếng Anh Chỉ Sau 3 Tháng</h6>
                <button className="btn-main link-btn-main">Tư vấn miễn phí ngay >></button>
            </div>
        </React.Fragment>
    );
};

export default IntroduceReviewFaq;