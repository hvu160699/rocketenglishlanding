import React, { useEffect, useState } from 'react';
import logo from "../../../assets/images/logo.png"
import "./header_layout.scss"
import { Link, NavLink } from "react-router-dom"
import * as path from "../../../config/router.config"
import * as API from "../../../api/header/header_api"

const Header = () => {

  const [menus, setMenus] = useState([])

  useEffect(() => {

    API.getAllMenu()
      .then(data => {
        try {
          setMenus(data)
        }
        catch (err) {
          console.log(err)
        }
      })
      .catch(err => console.log(err))

    const header = document.getElementById("header")
    let lastScrollTop = 0;
    document.addEventListener("scroll", function () {
      var st = document.body.scrollTop || document.documentElement.scrollTop;

      (st > 200 && st >= lastScrollTop) ? header.classList.add("hide") : header.classList.remove("hide")
      lastScrollTop = st <= 200 ? 200 : st;
    }, false);

    return () => {
      document.removeEventListener("scroll")
    };
  }, [])

  return (
    <nav className="navbar navbar-expand-lg fixed-top" id="header">
      <div className="container">
        <Link className="navbar-brand" to={path.redirect_to_home()}>
          <img className="img-fluid" src={logo} alt="logo" />
        </Link>
        <i className="fas fa-bars d-lg-none" style={{ color: 'white', fontSize: "24px", marginRight: "15px" }} data-toggle="collapse"
          data-target="#collapsibleNavId" aria-controls="collapsibleNavId" aria-expanded="false" aria-label="Toggle navigation" />
        <div className="collapse navbar-collapse" id="collapsibleNavId">
          <ul class="navbar-nav">
            <li className="nav-item">
              <Link className="nav-link" to={path.redirect_to_story()}>
                Câu chuyện thành công
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={path.redirect_to_tips()}>
                Tip học tiếng anh
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={path.redirect_to_review()}>
                Đánh giá
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={path.redirect_to_order()}>Đặt hàng ngay</Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Header;