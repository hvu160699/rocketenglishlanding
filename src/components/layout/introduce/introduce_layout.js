import React from 'react';
import "./introduce_layout.scss"
import arrow_button from "../../../assets/images/arrow_button.png"
import {Link} from "react-router-dom"
import {redirect_to_order} from "../../../config/router.config"

const Introduce = (text1, text2) => {
    return (
        <div className="container-button d-none d-lg-block">
            <div className="container">
                <div className="row">
                    <div className="col-lg-7 col-12 text-center ">
                        <strong>
                            {text1 ? text1 : "Nói Tiếng Anh Trôi Chảy Chỉ Sau 3 Tháng Tự Học!"}
                        </strong>
                        <p className="mt-3">
                            {text2 ? text2 : "Chương trình đang được hơn 67.200 học viên tại khắp 63 tỉnh thành theo học."}
                        </p>
                    </div>
                    <div className="col-lg-4 offset-lg-1 col-12 text">
                        <button className="btn-main"> <Link to={redirect_to_order()}>Đặt Hàng Ngay</Link></button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Introduce;