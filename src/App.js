import React from 'react';
import { BrowserRouter, Switch, Route, Router } from "react-router-dom"
import Header from "./components/layout/header/header_layout"
import Footer from "./components/layout/footer/footer_layout"
import RouterMain from "./config/router.main.config"
import BackToTop from "./components/layout/back_to_top/back_to_top"
import "./style.scss";
import 'antd/dist/antd.css';

const App = () => {
  return (
    <BrowserRouter>
      <Header />
      <div id="main-content" style={{paddingTop:'60px'}}>
        {RouterMain}
      </div>
      <Footer />
      <BackToTop />
    </BrowserRouter>
  );
};

export default App;