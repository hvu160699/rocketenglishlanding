import * as linkAPI from "../../config/linkAPI.config"
import { callApiC } from "../../helpers/api.helper"

export function getAllNewCourses() {
    return new Promise((resolve, reject) => {
      callApiC(linkAPI.GET_ALL_COURSES(), "GET")
        .then(data => {
          resolve(data.Response);
        })
        .catch(err => {
          reject(err);
        });
    });
  }