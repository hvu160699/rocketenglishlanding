import * as linkAPI from "../../config/linkAPI.config"
import { callApi } from "../../helpers/api.helper"


export function getPostByMenu(id) {
    return new Promise((resolve, reject) => {
        callApi(linkAPI.GET_POST_BY_MENU(id), "GET")
            .then(data => {
                resolve(data.Response);
            })
            .catch(err => {
                reject(err);
            });
    });
}


export function getPostByMetaTitle(metaTitle) {
    return new Promise((resolve, reject) => {
        callApi(linkAPI.GET_POST_BY_META(metaTitle), "GET")
            .then(data => {
                resolve(data.Response);
            })
            .catch(err => {
                reject(err);
            });
    });
}


export function getPostById(id) {
    return new Promise((resolve, reject) => {
        callApi(linkAPI.GET_POST_BY_ID(id), "GET")
            .then(data => {
                resolve(data.Response);
            })
            .catch(err => {
                reject(err);
            });
    });
}




