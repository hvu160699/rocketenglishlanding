import * as linkAPI from "../../config/linkAPI.config"
import { callApi } from "../../helpers/api.helper"

export function postContact(body) {
    return new Promise((resolve, reject) => {
        callApi(linkAPI.POST_CONTACT(), "POST", body)
            .then(data => {
                resolve(data.Response);
            })
            .catch(err => {
                reject(err);
            });
    });
}