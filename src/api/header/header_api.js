import * as linkAPI from "../../config/linkAPI.config"
import { callApi } from "../../helpers/api.helper"

export function getAllMenu() {
  return new Promise((resolve, reject) => {
    callApi(linkAPI.GET_ALL_MENU(), "GET")
      .then(data => {
        resolve(data.Response);
      })
      .catch(err => {
        reject(err);
      });
  });
}


export function getMenuByMetaTitle(meta) {
  return new Promise((resolve, reject) => {
    callApi(linkAPI.GET_MENU_BY_META(meta), "GET")
      .then(data => {
        resolve(data.Response);
      })
      .catch(err => {
        reject(err);
      });
  });
}


export function getMenuById(id) {
  return new Promise((resolve, reject) => {
    callApi(linkAPI.GET_MENU_BY_ID(id), "GET")
      .then(data => {
        resolve(data.Response);
      })
      .catch(err => {
        reject(err);
      });
  });
}