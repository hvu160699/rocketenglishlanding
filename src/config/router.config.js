export const home = "/";
export const redirect_to_home = () => "/";


export const order = "/dat-hang";
export const redirect_to_order = () => `/dat-hang`;

export const review = "/danh-gia";
export const redirect_to_review = () => `/danh-gia`;


export const contact = "/lien-he";
export const redirect_to_contact = () => `/lien-he`;

export const story = "/cau-chuyen-thanh-cong";
export const redirect_to_story = () => `/cau-chuyen-thanh-cong`;

export const story_detail = "/cau-chuyen-thanh-cong/:metaTitle";
export const redirect_to_story_detail = metaTitle => `/cau-chuyen-thanh-cong/${metaTitle}`;

export const tips = "/tip-hoc-tieng-anh";
export const redirect_to_tips = () => `/tip-hoc-tieng-anh`;

export const tips_detail = "/tip-hoc-tieng-anh/:metaTitle";
export const redirect_to_tips_detail = metaTitle => `/tip-hoc-tieng-anh/${metaTitle}`;

export const faq = "/faq";
export const redirect_to_faq = () => `/faq`;


