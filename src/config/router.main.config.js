import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom"
import * as path from "./router.config";
import HomePage from "../components/pages/home/home_page"
import OrderPage from "../components/pages/order/order_page"
import ContactPage from "../components/pages/contact/contact_page"
import ReviewPage from "../components/pages/review/review_page"
import StoryPage from "../components/pages/story/story_page"
import StoryDetailPage from "../components/pages/story/story_detail_page/story_detail_page"
import FaqPage from "../components/pages/faq/faq_page"
import Page404 from "../components/pages/404/page_404"
import ScrollToTopRoute from "./router.scrollToTop"
import TipsPage from "../components/pages/tips/tips_page";
import TipsDetailPage from "../components/pages/tips/tips_detail/tips_detail_page";
import NewHome from "../components/pages/home/new-home/";
const RouterIndex = (
        <Switch>
                <ScrollToTopRoute exact path={path.home} component={NewHome} />
                {/* <ScrollToTopRoute exact path='/new-home' component={NewHome} /> */}
                <ScrollToTopRoute exact path={path.order} component={OrderPage} />
                <ScrollToTopRoute exact path={path.review} component={ReviewPage} />
                <ScrollToTopRoute exact path={path.contact} component={ContactPage} />
                <ScrollToTopRoute exact path={path.faq} component={FaqPage} />
                <ScrollToTopRoute exact path={path.story} component={StoryPage} />
                <ScrollToTopRoute exact path={path.story_detail} component={StoryDetailPage} />
                <ScrollToTopRoute exact path={path.tips} component={TipsPage} />
                <ScrollToTopRoute exact path={path.tips_detail} component={TipsDetailPage} />
                <ScrollToTopRoute component={Page404} />
        </Switch>
);

export default RouterIndex;