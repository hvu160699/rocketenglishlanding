export const HOST_API = "http://english-api.tesosoft.com/api/landing/"
export const HOST_API_C = "https://english-api.ygame.vn/api/"
//  export const HOST_API_C = "http://english-api.tesosoft.com/api/";
export const HOST_STATIC = "http://static.tesosoft.com/english/"

export const GET_ALL_MENU = () => "menu/get-all";

export const GET_MENU_BY_ID = id => `menu/get-by-id/${id}`;

export const GET_MENU_BY_META = metatile => `menu/get-by-metatile/${metatile}`

export const GET_POST_BY_ID = id => `post/get-by-id/${id}`;

export const GET_POST_BY_META = metatile => `post/get-by-metatile/${metatile}`

export const GET_POST_BY_MENU = idMenu => `post/get-by-menu/${idMenu}`

export const GET_ALL_COURSES = () => `course/course-get-all`

export const POST_CONTACT = () => 'create-contact'
// body:{
//     "fullName":"abc",
//     "address":"address",
//     "phone":"phone",
//     "comment":"comment",
//     "district":"district",
//     "city":"city",
//     "course":"course"
//  }
