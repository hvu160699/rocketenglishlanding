import { HOST_API ,HOST_API_C } from "../config/linkAPI.config"


export function callApi(
  endpoint,
  method = "GET",
  body,
  header = {
    Accept: "application/json",
    "Content-Type": "application/json",
  }
) {


  return new Promise((resolve, reject) => {
    fetch(`${HOST_API}${endpoint}`, {
      headers: header,
      method,
      body: JSON.stringify(body)
    })
      .then(async response => {
        const json = await response.json();
        return { json, response };
      })
      .then(({ json, response }) => {

        if (!response.ok || json.ErrorCode !== 0) {
          reject({ status: response.status, msg: json });
        }

        return resolve(json);
      })
      .catch(error => {
        console.log(error);

        reject(error);
      });
  });
}


export function callApiC(
  endpoint,
  method = "GET",
  body,
  header = {
    Accept: "application/json",
    "Content-Type": "application/json",
  }
) {


  return new Promise((resolve, reject) => {
    fetch(`${HOST_API_C}${endpoint}`, {
      headers: header,
      method,
      body: JSON.stringify(body)
    })
      .then(async response => {
        const json = await response.json();
        return { json, response };
      })
      .then(({ json, response }) => {

        if (!response.ok || json.ErrorCode !== 0) {
          reject({ status: response.status, msg: json });
        }

        return resolve(json);
      })
      .catch(error => {
        console.log(error);

        reject(error);
      });
  });
}
