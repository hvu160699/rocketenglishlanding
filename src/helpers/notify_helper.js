

import React, { Component } from 'react'
import Swal from 'sweetalert2'

export const error = text => {
    {
        Swal.fire({
            position: 'center',
            type: 'error',
            title: `${text}`,
            showConfirmButton: false,
            timer: 2500
        })
    }
}


export const successPost = text => {
    Swal.fire({
        position: 'center',
        type: 'success',
        text: 'Chúng tôi đã nhận được thông tin của bạn và sẽ liên hệ với bạn trong thời gian sớm nhất.',
        title: `${text}`,
        showConfirmButton: false,
        timer: 2500
    })


}

export const success = text => {
    Swal.fire({
        title: text,
        type: 'success',
        showConfirmButton: false,
        timer: 2500
    })
}