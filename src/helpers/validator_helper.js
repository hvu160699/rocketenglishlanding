import React from 'react'
import { isEmail, isEmpty } from 'validator'

export const required = (value) => {
    if (isEmpty(value)) {
        return <small className="form-text text-danger validator">Vui lòng nhập thông tin</small>;
    }
}

export const email = (value) => {
    if (!isEmail(value)) {
        return <small className="form-text text-danger validator">{value} không đúng định dạng</small>;
    }
}

export const minLength = (value) => {
    if (value.trim().length < 6) {
        return <small className="form-text text-danger validator">Tối thiểu 6 ký tự.</small>;
    }
}

export const nullValidator = (value) => {
    return null;
}

export const validatorEmail = 'Email không hợp lệ'

export const validatorRequired = 'Vui lòng nhập thông tin'

